package c.mars.rx.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import c.mars.rx.fragments.BaseFragment;
import c.mars.rx.fragments.BlockingFragment;
import c.mars.rx.fragments.CombineFragment;
import c.mars.rx.fragments.ConditionalFragment;
import c.mars.rx.fragments.CreateFragment;
import c.mars.rx.fragments.ErrorFragment;
import c.mars.rx.fragments.FilteringFragment;
import c.mars.rx.fragments.MathFragment;
import c.mars.rx.fragments.SchedulersFragment;
import c.mars.rx.fragments.SubjectFragment;

/**
 * Created by Constantine Mars on 9/6/15.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {
    private BaseFragment[] fragments = {
            CreateFragment.newInstance(),
            BlockingFragment.newInstance(),
            ConditionalFragment.newInstance(),
            FilteringFragment.newInstance(),
            CombineFragment.newInstance(),
            ErrorFragment.newInstance(),
            MathFragment.newInstance(),
            SchedulersFragment.newInstance(),
            SubjectFragment.newInstance(),
    };

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position >= fragments.length || position < 0) {
            return null;
        }

        return (Fragment) fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position >= fragments.length || position < 0) {
            return null;
        }

        return fragments[position].getTitle();
    }
}
