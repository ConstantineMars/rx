package c.mars.rx.utils;

import android.app.Activity;
import android.widget.TextView;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import timber.log.Timber;

/**
 * Created by Constantine Mars on 9/4/15.
 */
@Data
@RequiredArgsConstructor
public class Display {
    private final Activity activity;
    private final TextView textView;

    public void show(Integer i) {
        show(String.valueOf(i));
    }
    public void show(String message) {
        activity.runOnUiThread(() -> {
            Timber.d(message);
            textView.append("\n" + message);
        });
    }

    public void clear() {
        textView.setText("");
    }
}
