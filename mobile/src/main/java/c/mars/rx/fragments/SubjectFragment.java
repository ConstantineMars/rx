package c.mars.rx.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import c.mars.rx.R;
import c.mars.rx.rx.SubjectEx;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubjectFragment extends Fragment implements BaseFragment {

    private SubjectEx ex = new SubjectEx();
    private OnFragmentInteractionListener listener;

    public SubjectFragment() {
        // Required empty public constructor
    }

    public static BaseFragment newInstance() {
        return new SubjectFragment();
    }

    @OnClick(R.id.p)
    void p() {
        ex.publish();
    }

    @OnClick(R.id.r)
    void r() {
        ex.replay();
    }

    @OnClick(R.id.a)
    void a() {
        ex.async();
    }

    @OnClick(R.id.b)
    void b() {
        ex.behaviour();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subject, container, false);
        ButterKnife.bind(this, view);
        ex.setDisplay(listener.getDisplay());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnFragmentInteractionListener) context;
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    @Override
    public String getTitle() {
        return "Subject";
    }
}
