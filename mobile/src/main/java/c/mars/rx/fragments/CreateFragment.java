package c.mars.rx.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jakewharton.rxbinding.view.RxView;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import c.mars.rx.R;
import c.mars.rx.rx.CreateEx;
import c.mars.rx.utils.Display;


public class CreateFragment extends Fragment implements BaseFragment {

    CreateEx e = CreateEx.getInst();

    @Bind(R.id.click)
    Button button;
    int counter = 0;
    private Display display;
    private OnFragmentInteractionListener mListener;

    public CreateFragment() {
        // Required empty public constructor
    }

    public static CreateFragment newInstance() {
        return new CreateFragment();
    }

    @OnClick(R.id.collection)
    void c() {
        display.clear();
        e.from();
    }

    @OnClick(R.id.t)
    void t() {
        display.clear();
        e.timer();
    }

    @OnClick(R.id.n)
    void n() {
        display.clear();
        e.retrofit();
    }

    @OnClick(R.id.u)
    void u() {
//        rxbindings example
        display.clear();

        RxView.clicks(button)
                .map(v -> ++counter)
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(c -> display.show("button " + c));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create, container, false);
        ButterKnife.bind(this, v);

        display = mListener.getDisplay();
        e.setDisplay(display);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public String getTitle() {
        return "Create";
    }
}
