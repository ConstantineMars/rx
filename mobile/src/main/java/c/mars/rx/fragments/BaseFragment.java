package c.mars.rx.fragments;

/**
 * Created by Constantine Mars on 9/7/15.
 */
public interface BaseFragment {
    String getTitle();
}
