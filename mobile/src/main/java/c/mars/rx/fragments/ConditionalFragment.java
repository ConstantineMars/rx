package c.mars.rx.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import c.mars.rx.R;
import c.mars.rx.rx.ConditionalEx;
import c.mars.rx.utils.Display;

public class ConditionalFragment extends Fragment implements BaseFragment {

    ConditionalEx ex = ConditionalEx.getInstance();
    private OnFragmentInteractionListener mListener;
    private Display display;

    public ConditionalFragment() {
        // Required empty public constructor
    }

    public static ConditionalFragment newInstance() {
        return new ConditionalFragment();
    }

    @OnClick(R.id.ie)
    void defaultIfEmpty() {
        display.clear();
        ex.defaultIfEmpty();
    }

    @OnClick(R.id.c)
    void c() {
        display.clear();
        ex.contains();
    }

    @OnClick(R.id.u)
    void u() {
        display.clear();
        ex.takeUntil();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_conditionals, container, false);
        ButterKnife.bind(this, v);
        display = mListener.getDisplay();
        ex.setDisplay(display);
        return v;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public String getTitle() {
        return "Conditional";
    }


}
