package c.mars.rx.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import c.mars.rx.R;
import c.mars.rx.rx.SchedulersEx;

/**
 * A simple {@link Fragment} subclass.
 */
public class SchedulersFragment extends Fragment implements BaseFragment {

    private SchedulersEx ex = new SchedulersEx();
    private OnFragmentInteractionListener listener;

    public SchedulersFragment() {
        // Required empty public constructor
    }

    public static SchedulersFragment newInstance() {
        return new SchedulersFragment();
    }

    @OnClick(R.id.c)
    void c() {
        listener.getUnsafeTextView().setText("");
        ex.computations();
    }

    @OnClick(R.id.io)
    void io() {
        listener.getUnsafeTextView().setText("");
        ex.io(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_schedulers, container, false);
        ButterKnife.bind(this, view);
        ex.setTextView(listener.getUnsafeTextView());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnFragmentInteractionListener) context;
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }


    @Override
    public String getTitle() {
        return "Schedulers";
    }
}
