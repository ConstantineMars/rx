package c.mars.rx.fragments;

import android.widget.TextView;

import c.mars.rx.utils.Display;

/**
 * Created by Constantine Mars on 9/7/15.
 */
public interface OnFragmentInteractionListener {
    Display getDisplay();

    TextView getUnsafeTextView();
}
