package c.mars.rx.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import c.mars.rx.R;
import c.mars.rx.rx.CombineEx;
import c.mars.rx.utils.Display;

public class CombineFragment extends Fragment implements BaseFragment {

    CombineEx ex = CombineEx.getInstance();
    private Display display;
    private OnFragmentInteractionListener mListener;

    public CombineFragment() {
        // Required empty public constructor
    }

    public static CombineFragment newInstance() {
        CombineFragment fragment = new CombineFragment();
        return fragment;
    }

    @OnClick(R.id.m)
    void m() {
        display.clear();
        ex.merge();
    }

    @OnClick(R.id.z)
    void z() {
        display.clear();
        ex.zip();
    }

    @OnClick(R.id.j)
    void j() {
        display.clear();
        ex.combineLatest();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_combine, container, false);
        ButterKnife.bind(this, v);

        display = mListener.getDisplay();
        ex.setDisplay(display);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public String getTitle() {
        return "Combine";
    }
}
