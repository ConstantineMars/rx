package c.mars.rx.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import c.mars.rx.R;
import c.mars.rx.rx.FilterEx;
import c.mars.rx.utils.Display;

/**
 * Created by Constantine Mars on 9/7/15.
 */
public class FilteringFragment extends Fragment implements BaseFragment {
    FilterEx ex = FilterEx.getInstance();
    private Display d;
    private OnFragmentInteractionListener listener;

    public FilteringFragment() {
    }

    public static final FilteringFragment newInstance() {
        return new FilteringFragment();
    }

    @OnClick(R.id.d)
    void d() {
        d.clear();
        ex.debounce();
    }
    @OnClick(R.id.f)
    void f() {
        d.clear();
        ex.filter();
    }

    @OnClick(R.id.s)
    void s() {
        d.clear();
        ex.sample();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_filter, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        listener = (OnFragmentInteractionListener) context;
        d = listener.getDisplay();
        ex.setDisplay(d);
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    @Override
    public String getTitle() {
        return "Filtering";
    }
}
