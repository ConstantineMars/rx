package c.mars.rx.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import c.mars.rx.R;
import c.mars.rx.rx.BlockingEx;
import c.mars.rx.utils.Display;


public class BlockingFragment extends Fragment implements BaseFragment {
    private Display display;
    private BlockingEx ex = BlockingEx.getInstance();
    private OnFragmentInteractionListener mListener;

    public BlockingFragment() {
    }

    public static BlockingFragment newInstance() {
        return new BlockingFragment();
    }

    @OnClick(R.id.b)
    void b() {
        display.clear();
        ex.max();
    }

    @OnClick(R.id.fe)
    void fe() {
        display.clear();
        ex.forEach();
    }

    @OnClick(R.id.sorted)
    void sorted() {
        display.clear();
        ex.toIterable();
    }

    @OnClick(R.id.single)
    void single() {
        display.clear();
        ex.single();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_blocking, container, false);
        ButterKnife.bind(this, v);
        display = mListener.getDisplay();
        ex.setDisplay(display);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public String getTitle() {
        return "Blocking";
    }
}
