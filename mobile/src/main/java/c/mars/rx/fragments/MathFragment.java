package c.mars.rx.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import c.mars.rx.R;
import c.mars.rx.rx.MathEx;

public class MathFragment extends Fragment implements BaseFragment {
    private MathEx ex = new MathEx();
    private c.mars.rx.fragments.OnFragmentInteractionListener mListener;

    public MathFragment() {
        // Required empty public constructor
    }

    public static MathFragment newInstance() {
        return new MathFragment();
    }

    @OnClick(R.id.a)
    void a() {
        mListener.getDisplay().clear();
        ex.avg();
    }

    @OnClick(R.id.r)
    void r() {
        mListener.getDisplay().clear();
        ex.reduce();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        ButterKnife.bind(this, view);
        ex.setDisplay(mListener.getDisplay());
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public String getTitle() {
        return "Math";
    }

}
