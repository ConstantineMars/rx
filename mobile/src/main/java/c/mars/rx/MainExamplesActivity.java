package c.mars.rx;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import c.mars.rx.adapters.TabsPagerAdapter;
import c.mars.rx.fragments.OnFragmentInteractionListener;
import c.mars.rx.utils.Display;
import timber.log.Timber;

public class MainExamplesActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    @Bind(R.id.vp)
    ViewPager vp;
    @Bind(R.id.tabs)
    TabLayout tabs;
    @Bind(R.id.t)
    TextView t;
    private Display d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_examples);
        Timber.plant(new Timber.DebugTree());
        ButterKnife.bind(this);

        t.setMovementMethod(new ScrollingMovementMethod());

        d = new Display(this, t);
        TabsPagerAdapter pagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(pagerAdapter);
        tabs.setupWithViewPager(vp);
    }

    @Override
    public Display getDisplay() {
        return d;
    }

    @Override
    public TextView getUnsafeTextView() {
        return t;
    }
}
