package c.mars.rx.rx;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Constantine Mars on 10/10/15.
 */
public class ErrorEx extends BaseExample {

    private boolean failedOnce = false;

    private ErrorEx() {
    }

    public static ErrorEx getInstance() {
        return SH.INSTANCE;
    }

    public void retry() {
        rx.Observable<Integer> canFail = rx.Observable.create(new Observable.OnSubscribe<Integer>() {

            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                for (int i = 0; i < 6; i++) {
                    switch (i) {
                        case 3:
                            if (!failedOnce) {
                                failedOnce = true;
                                subscriber.onError(new Error());
                                return;
                            }
                            break;
                        case 5:
                            subscriber.onError(new Throwable());
                            return;
                    }
                    subscriber.onNext(i);
                }
                subscriber.onCompleted();
            }
        });
        canFail.retry((integer, throwable) -> {
            boolean retry = (throwable instanceof Error);
            getDisplay().show("retry, errors: " + integer);
            return retry;
        })
                .subscribe(i -> {
                    getDisplay().show(i);
                }, throwable -> {
                    getDisplay().show("error: " + throwable.getMessage());
                });
    }

    static class Error extends Throwable {

    }

    private static class SH {
        private static final ErrorEx INSTANCE = new ErrorEx();
    }
}
