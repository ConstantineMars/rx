package c.mars.rx.rx;

import android.util.Pair;

import java.util.concurrent.TimeUnit;

/**
 * Created by Constantine Mars on 9/7/15.
 */
public class FilterEx extends BaseExample {

    private FilterEx() {
    }

    public static FilterEx getInstance() {
        return SH.INSTANCE;
    }

    public void debounce() {
        long[] times = {3, 2, 1, 5, 2, 6};
        rx.Observable<Pair<Integer, Long>> observable = rx.Observable.create(subscriber -> {
            int sz = times.length;
            for (int i = 0; i < sz; i++) {
                try {
                    long t = times[i];
                    TimeUnit.MILLISECONDS.sleep(t);
                    subscriber.onNext(new Pair<>(i, t));
                } catch (InterruptedException e) {
                    subscriber.onError(e);
                }
            }
            subscriber.onCompleted();
        });
        observable.debounce(4, TimeUnit.MILLISECONDS)
                .subscribe(pair -> getDisplay().show("out: value=" + pair.first + ", time=" + pair.second));
    }

    public void sample() {
        long[] times = {3, 2, 1, 5, 4, 3, 1};
        rx.Observable<Pair<Integer, Long>> observable = rx.Observable.create(subscriber -> {
            int sz = times.length;
            for (int i = 0; i < sz; i++) {
                try {
                    long t = times[i];
                    TimeUnit.MILLISECONDS.sleep(t * 10);
                    subscriber.onNext(new Pair<>(i, t));
                } catch (InterruptedException e) {
                    subscriber.onError(e);
                }
            }
            subscriber.onCompleted();
        });
        observable.sample(40, TimeUnit.MILLISECONDS)
                .subscribe(pair -> getDisplay().show("out: value=" + pair.first + "; time=" + pair.second));
    }

    public void filter() {
        rx.Observable.range(0, 10)
                .filter(i -> i > 5)
                .forEach(i -> getDisplay().show(i));
    }

    private static class SH {
        private static final FilterEx INSTANCE = new FilterEx();
    }
}
