package c.mars.rx.rx;

import rx.observables.MathObservable;

/**
 * Created by Constantine Mars on 10/10/15.
 */
public class MathEx extends BaseExample {
    public void avg() {
        rx.Observable<Integer> integers = rx.Observable.range(0, 10);
        MathObservable.averageInteger(integers).subscribe(avg -> {
            getDisplay().show(avg);
        });
    }

    public void reduce() {
        rx.Observable.range(0, 10).reduce((a, b) -> {
            int c = a + b;
            getDisplay().show("reduce: a=" + a + " + " + b + " = " + c);
            return c;
        }).forEach(value -> getDisplay().show("result: " + value));
    }

    String someFun(int value, String name) {
        return name + " has value " + value;
    }

    void a() {


        // Java 7 function
        String s = someFun(1, "Fun");


        // Java 7 functional interface
        FunInterface f = new FunInterface() {
            @Override
            public String haveFun(int value, String name) {
                return someFun(value, name);
            }
        };

        String s1 = f.haveFun(2, "NewFun");

        // Java 8 lambdas
        FunInterface f1 = (value, name) -> someFun(value, name);

        String s2 = f1.haveFun(3, "ModernFun");


        // Java 8 lambdas with functional references
        FunInterface f2 = this::someFun;

        String s3 = f2.haveFun(4, "EvenMoreFun");

    }

    interface FunInterface {
        String haveFun(int value, String name);
    }


}
