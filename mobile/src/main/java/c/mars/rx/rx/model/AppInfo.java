package c.mars.rx.rx.model;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Created by Constantine Mars on 8/31/15.
 */
@Data @AllArgsConstructor
public class AppInfo implements Comparable<AppInfo> {
    long lastUpdateTime;
    String name;
    Drawable icon;

    @Override
    public int compareTo(AppInfo another) {
        return name.compareTo(another.getName());
    }

    @Data @RequiredArgsConstructor
    public static class AppInfoFactory{
        private final PackageManager packageManager;

        public AppInfo createAppInfo(ResolveInfo info){
            return new AppInfo(System.currentTimeMillis(), info.loadLabel(packageManager).toString(), info.loadIcon(packageManager));
        }
    }
}
