package c.mars.rx.rx;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import c.mars.rx.utils.Display;
import lombok.Data;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Constantine Mars on 6/17/15.
 * examples of rx usage
 */
@Data
public class CreateEx {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    final Random random = new Random();
    private final int N = 5;
    private final int MAX = 100;
    protected Display display;

    private CreateEx() {
    }

    public static CreateEx getInst() {
        return SH.I;
    }

    public void createInJava() {
//        create observable and subscribe for it in old plain Java

        rx.Observable<Integer> observable = rx.Observable.create(
                new rx.Observable.OnSubscribe<Integer>() {
                    @Override
                    public void call(Subscriber<? super Integer> subscriber) {
                        for (int i = 0; i < N; i++) {
                            Integer integer = random.nextInt(MAX);
                            subscriber.onNext(integer);
                        }
                        subscriber.onCompleted();
                    }
                });

        rx.Observer<Integer> observer = new rx.Observer<Integer>() {
            @Override
            public void onCompleted() {
                display.show("completed");
            }

            @Override
            public void onError(Throwable e) {
                display.show("error: " + e.getMessage());
            }

            @Override
            public void onNext(Integer integer) {
                display.show("next: " + integer);
            }
        };

        Subscription subscription = observable.subscribe(observer);

        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }


    }

    public void createWithLambdas() {
//        base example rewritten with lambdas

        rx.Observable<Integer> observable = rx.Observable.create(subscriber -> {
            for (int i = 0; i < N; i++)
                subscriber.onNext(random.nextInt(MAX));

            subscriber.onCompleted();
        });

        observable.subscribe(
                integer -> display.show("next: " + integer),
                throwable -> display.show("error: " + throwable.getMessage()),
                () -> display.show("completed"));
    }

    public void createSimplified() {
//        subscribe instantly and only for data
        rx.Observable.create(subscriber -> {
            for (int i = 0; i < N; i++) subscriber.onNext(random.nextInt(MAX));
            subscriber.onCompleted();
        }).subscribe(integer -> display.show("next: " + integer));
    }

    private rx.Observable<Integer> create() {
//        return observable to subscribe for it's output from anywhere
        final int MAX_N = random.nextInt(12);
        return rx.Observable.create(subscriber -> {
            for (int i = 0; i < MAX_N; i++) subscriber.onNext(random.nextInt(MAX));
            subscriber.onCompleted();
        });
    }

    private rx.Observable<Integer> createWithError() {
//        return observable to subscribe for it's output from anywhere
        return rx.Observable.create(subscriber -> {
            for (int i = 0; i < 10; i++) {
                if (i == 4) subscriber.onError(new Throwable("observable fail on element 4"));
                subscriber.onNext(random.nextInt(MAX));
            }
            subscriber.onCompleted();
        });
    }

    public void subscription() {
        rx.Subscription subscription = create().subscribe(integer -> display.show("next+ " + integer));
        subscription.unsubscribe();
    }

    public void from() {
//        from() creates observable from collection
        ArrayList<Integer> arrayList = new ArrayList<>();
        int MAX_N = random.nextInt(12) + 5;
        for (int i = 0; i < MAX_N; i++) arrayList.add(random.nextInt(MAX));

        rx.Observable.from(arrayList)
                .subscribe(
                        integer -> display.show("next: " + integer),
                        throwable -> display.show("error: " + throwable.getMessage()),
                        () -> display.show("complete"));
    }

    private List<Integer> generate() {
        Random r = new Random();
        int n = r.nextInt(5) + 5;
        ArrayList<Integer> a = new ArrayList<>();

        for (int i = 0; i < n; i++)
            a.add(r.nextInt(100));
        return a;
    }

    public void just() {
        rx.Observable.just(generate())
                .subscribe(integer -> display.show("next: " + integer),
                        throwable -> display.show("error: " + throwable.getMessage()),
                        () -> display.show("complete"));
    }

    public void fromWithTiming() {
//        to make sense of from - we just make observable emitting items with timeout
        ArrayList<Integer> arrayList = new ArrayList<>();
        int MAX_N = random.nextInt(20) + 4;
        for (int i = 0; i < MAX_N; i++) arrayList.add(random.nextInt(MAX));

        final int TIMEOUT = 1_000;

        new Thread(() -> {
//            if we try to run this RxJava code from Android UI Thread - we'll block it.
//            for sake of example let's make this time-consuming work aynchronous in old plain Java way
//            (later we'll do this properly - with schedulers)
            rx.Observable.from(arrayList)
                    .map(integer -> {
                        try {
                            Thread.sleep(TIMEOUT);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return integer;
                    })
                    .subscribe(integer -> display.show("next: " + integer),
                            throwable -> display.show("error: " + throwable.getMessage()),
                            () -> display.show("completed"));
        }).start();

    }

    public void toList() {
        create().toList().subscribe(list -> {
            display.show(list.toString());
        });
    }

    public void timer() {
//        interval repeats timing. map helps convert tick to data
        Random r = new Random();
        rx.Observable.interval(2, TimeUnit.SECONDS)
                .map(t -> new long[]{t, r.nextInt(100)})
                .limit(5)
                .subscribe(
                        tuple -> display.show("next: " + Arrays.toString(tuple)),
                        throwable -> {
                        },
                        () -> display.show("complete"));
    }

    public void retrofit() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.github.com")
                .build();

        WeatherApi.GitHubService service = restAdapter.create(WeatherApi.GitHubService.class);

        service.listRepos("c-mars")
                .flatMap(Observable::from)
                .limit(10)
                .subscribe(repo -> display.show("next: " + repo.toString()),
                        throwable -> display.show("error: " + throwable.getMessage()),
                        () -> display.show("completed"));

    }

    //    interface for Retrofit auto parsing of server response
    interface WeatherApi {

        interface GitHubService {
            @GET("/users/{user}/repos")
            rx.Observable<List<Repo>> listRepos(@Path("user") String user);
        }


    }

    private static class SH {
        private static final CreateEx I = new CreateEx();
    }

    //    custom structure for parsing JSON, representing response
    @Data
    class Repo {
        public String name;
        public String updated_at;
        public Integer watchers_count;

        public String getUpdated_at() {
            Date date = new org.joda.time.DateTime(updated_at).toDate();
            return DATE_FORMAT.format(date);
        }

        public String toString() {
            return name + " [updated: " + getUpdated_at() + "]";
        }
    }
}
