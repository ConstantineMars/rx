package c.mars.rx.rx;

import java.util.Random;

/**
 * Created by Constantine Mars on 9/7/15.
 */
public class ConditionalEx extends BaseExample {
    private ConditionalEx() {
    }

    public static ConditionalEx getInstance() {
        return SH.INSTANCE;
    }

    public void contains() {
        Random r = new Random();

        int max = new Random().nextInt(4);
        rx.Observable.range(0, max)
                .contains(2)
                .forEach(i -> getDisplay().show(
                        "range: " + 0 + "->" + max + ","
                                + "contains 2: " + String.valueOf(i)));




    }

    public void defaultIfEmpty() {
        Random r = new Random();
        int max = r.nextInt(4);


        rx.Observable.range(0, max)
                .defaultIfEmpty(999)
                .forEach(i -> getDisplay().show(
                        "range 0->" + max + ", "
                                + "value (999 if empty):" + String.valueOf(i)));



    }

    public void takeUntil() {
        rx.Observable.range(0, 10)
                .takeUntil(i -> i == 5)
                .forEach(i -> getDisplay().show(String.valueOf(i)));
    }

    private static class SH {
        private static final ConditionalEx INSTANCE = new ConditionalEx();
    }
}
