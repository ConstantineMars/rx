package c.mars.rx.rx;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import c.mars.rx.utils.Display;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import rx.observables.MathObservable;

/**
 * Created by Constantine Mars on 8/18/15.
 */
@Data
public class BlockingEx {

    private final AmmeterReadings[] data = {
            new AmmeterReadings(1, 0.5),
            new AmmeterReadings(9, 1),
            new AmmeterReadings(4, 1.5),
            new AmmeterReadings(6, 2),
            new AmmeterReadings(14, 2.5),
            new AmmeterReadings(5, 3)
    };
    private Display display;

    private BlockingEx() {
    }

    public static BlockingEx getInstance() {
        return SH.I;
    }

    private static float getMaxValue(AmmeterReadings[] data) {

        long maxValue = MathObservable.max(rx.Observable.from(data)
                .map(AmmeterReadings::getCurrent)
                .takeLast(5))
                .toBlocking().firstOrDefault(0L);


        return maxValue;

    }

    private String f2() {
        return String.format("%.2f", new Random().nextFloat());
    }

    public void forEach() {


//        asynchronous foreach (alias for onNext)
        rx.Observable.just(1, 904, 872, 10, 34)
                .forEach(i -> display.show("Observable.forEach:" + i));

//        blocking (synchronous) foreach
        rx.Observable.just(3, 5, 10, 24, 1)
                .toBlocking()
                .forEach(s -> display.show("BlockingObservable.forEach:" + s));



    }

    public void last() {
        Random r = new Random();


        int last = rx.Observable.timer(500, TimeUnit.MILLISECONDS)
                .map(t -> r.nextInt(100))
                .limit(5)
                .toBlocking().last();

        display.show("last:" + last);


    }

    public void single() {
        Random r = new Random();


        rx.Observable.range(0, 0)
                .singleOrDefault(-1)
                .forEach(i -> getDisplay().show("single:" + i));


    }

    public void toIterable() {


        final Integer[] data = {200, 4, 145, -1, 10, -12, 80};

        Iterable<Integer> iterable = rx.Observable.from(data)
                .toBlocking().toIterable();

        for (Integer i : iterable) {
            display.show("iterable:" + i.toString());
        }


        List<Integer> list = rx.Observable.from(data)
                .toSortedList()
                .toBlocking()
                .firstOrDefault(null);



        if (list != null) {
            for (Integer i : list) {
                display.show("sorted:" + i.toString());
            }
        }
    }

    public void max() {
        float max = getMaxValue(data);
        display.show("max: " + String.valueOf(max));
    }

    private static class SH {
        private static final BlockingEx I = new BlockingEx();
    }

    @Data
    @AllArgsConstructor
    private class Accum {
        private int value;
        private int sum;

        public void add(int v) {
            sum += v;
        }
    }

    @Data
    @RequiredArgsConstructor
    private class AmmeterReadings {
        private final long current;
        private final double time;
    }
}
