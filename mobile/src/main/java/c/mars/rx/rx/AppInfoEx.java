package c.mars.rx.rx;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import c.mars.rx.rx.model.AppInfo;
import c.mars.rx.utils.Display;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Created by Constantine Mars on 8/31/15.
 */
@Data @RequiredArgsConstructor
public class AppInfoEx {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("hh:mm:ss.SSS");
    private final Context context;
    private final Display display;

    public void defer(){
        display.show("deferring: "+DATE_FORMAT.format(System.currentTimeMillis()));

    }

    public rx.Observable<AppInfo> getAppsRx(){
        return rx.Observable.create(subscriber -> {
            AppInfo.AppInfoFactory appInfoFactory=new AppInfo.AppInfoFactory(context.getPackageManager());
            List<ResolveInfo> infos= getResolveInfos();
            for(int i=0, size=infos.size(); i<size; i++){
                ResolveInfo info=infos.get(i);
                AppInfo appInfo=appInfoFactory.createAppInfo(info);
                subscriber.onNext(appInfo);
            }

            subscriber.onCompleted();
        });
    }

    public List<AppInfo> getApps(){
        List<ResolveInfo> infos=getResolveInfos();

        AppInfo.AppInfoFactory appInfoFactory=new AppInfo.AppInfoFactory(context.getPackageManager());
        List<AppInfo> apps= new ArrayList<>();
        for(int i=0, size=infos.size(); i<size; i++){
            ResolveInfo info=infos.get(i);
            AppInfo appInfo=appInfoFactory.createAppInfo(info);
            apps.add(appInfo);
        }
        return apps;
    }

    private List<ResolveInfo> getResolveInfos(){
        Intent intent= new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        PackageManager packageManager=context.getPackageManager();
        List<ResolveInfo> infos=packageManager.queryIntentActivities(intent, 0);

        return infos;
    }
}
