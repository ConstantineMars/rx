package c.mars.rx.rx;

import rx.subjects.AsyncSubject;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.ReplaySubject;
import rx.subjects.Subject;

/**
 * Created by Constantine Mars on 8/16/15.
 */
public class SubjectEx extends BaseExample {

    //    publish subject, subscription point
    public void publish() {
        getDisplay().clear();
        PublishSubject<String> subject = PublishSubject.create();
        example(subject);
    }

    public void behaviour() {
        getDisplay().clear();
        BehaviorSubject subject = BehaviorSubject.create();
        example(subject);
    }

    public void replay() {
        getDisplay().clear();
        ReplaySubject<String> subject = ReplaySubject.createWithSize(2);
        example(subject);
    }

    public void async() {
        getDisplay().clear();
        AsyncSubject<String> subject = AsyncSubject.create();
        example(subject);
    }

    private void example(Subject<String, String> subject) {
        subject.onNext("before 1");
        subject.onNext("before 2");
        subject.onNext("before 3");
        subject.onNext("before 4");
        subject.subscribe(s -> getDisplay().show("subscribed: " + s));
        subject.onNext("after 5");
        subject.onNext("after 6");
        subject.onNext("after 7");
        subject.onNext("after 8");
        subject.onCompleted();
    }
}
