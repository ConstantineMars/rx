package c.mars.rx.rx;

import android.content.Context;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.observables.MathObservable;
import rx.schedulers.Schedulers;

/**
 * Created by Constantine Mars on 10/10/15.
 */
public class SchedulersEx extends BaseExample {
    private TextView textView;

    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public void io(Context context) {
        textView.setText("");
        rx.Observable.from(readFromFile(context))
                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
                .forEach(line -> textView.append("\n" + line));
    }

    private List<String> readFromFile(Context context) {
        List<String> lines = new ArrayList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("file.txt")));

            // do reading, usually loop until end of file reading
            String line = reader.readLine();
            while (line != null) {
                line = reader.readLine();
                lines.add(line);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return lines;
    }

    public void computations() {
        textView.setText("");
        rx.Observable source = rx.Observable.range(0, 10).map(integer -> {

            List<Integer> outs = new ArrayList<>();
            for (int i = 0; i < 1000; i++) {
                for (int j = 0; j < 1000; j++) {
                    outs.add((int) Math.pow(i, j));
                }
            }
            return outs;
        }).flatMap(Observable::from);

        MathObservable.sumInteger(source)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> textView.setText("final sum: " + integer.toString()));
    }
}
