package c.mars.rx.rx;

import c.mars.rx.utils.Display;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by Constantine Mars on 9/7/15.
 */
@Data
@RequiredArgsConstructor
public class BaseExample {
    @Getter
    private static final Integer[] data = {
            3, 1, -40, 20, 0
    };
    private Display display;
}
