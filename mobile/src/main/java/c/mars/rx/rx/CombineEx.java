package c.mars.rx.rx;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import timber.log.Timber;

/**
 * Created by Constantine Mars on 10/10/15.
 */
public class CombineEx extends BaseExample {

    private CombineEx() {
    }

    public static CombineEx getInstance() {
        return SH.INSTANCE;
    }

    public void merge() {
        rx.Observable first = Observable.range(0, 5); //int[]
        rx.Observable second = Observable.from(new String[]{"one", "two", "three", "four", "five"}); //String[]
        rx.Observable.merge(first, second).forEach(item ->
                getDisplay().show(item.toString()));
    }

    public void zip() {
        rx.Observable<Integer> first = Observable.range(0, 5); //int[]
        rx.Observable<String> second = Observable.from(new String[]{"one", "two", "three", "four", "five"}); //String[]
        rx.Observable.zip(first, second, (integer, s) -> new Pair(s, integer))
                .forEach(pair -> getDisplay().show(pair.toString()));
    }


    public void combineLatest() {
        List<String> d = new ArrayList<String>();
        for (int i = 0; i < 3; i++) d.add("-");
        rx.Observable<String> a = Observable.just("a").repeat(3);
        final int BUF_SZ = 3;
        rx.Observable<List<String>> b = Observable.just("b")
                .repeat(10)
                .buffer(BUF_SZ)
                .map(lst -> {
                    int sz = lst.size();
//                    fix list with mock placeholders
                    if (sz < BUF_SZ) {
                        for (int i = sz; i < BUF_SZ; i++) {
                            lst.add("-");
                        }
                    }
                    return lst;
                });

        rx.Observable.combineLatest(a, b, (s, lst) -> Observable.from(lst).startWith(s))
                .flatMap(v -> v)
                .forEach(v -> Timber.d(v.toString()));
    }

    private static class SH {
        private static final CombineEx INSTANCE = new CombineEx();
    }

    class Pair {
        String name;
        Integer value;

        public Pair(String name, Integer value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return name + " has value " + value;
        }
    }
}
